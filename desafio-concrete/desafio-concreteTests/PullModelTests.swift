//
//  PullModelTests.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 30/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import XCTest
@testable import desafio_concrete

class PullModelTests: XCTestCase, Parser, LoadJson {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        //Faz o teste com um JSON Normal
        let json = loadList(with: "PullRequest", bundleClass: PullModelTests.self)
        XCTAssertNotNil(json, "JSON INVÁLIDO")
        let model = parsePullRequest(response: json)
        XCTAssertNotNil(model, "PARSE DO REPOSITORIO ESTÁ INVALIDO")
        
        //Testa um arquivo de JSON quebrado, caso não sejá nil a resposta dos dois Asserts, o model está conseguindo parsear um JSON inválido.
        let jsonBroken = loadList(with: "PullRequestBroken", bundleClass: PullModelTests.self)
        XCTAssertNil(jsonBroken, "JSON INVALIDO")
        let modelBroken = parsePullRequest(response: jsonBroken)
        XCTAssertNil(modelBroken, "PARSE DO REPOSITORIO ESTA INVALIDO")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
