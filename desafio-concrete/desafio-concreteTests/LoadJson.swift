//
//  LoadJson.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 30/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation

protocol LoadJson {
    func loadJson(with path: String, bundleClass: AnyClass)-> [String: Any]?
    func loadList(with path: String, bundleClass: AnyClass)-> [[String: Any]]?
}

extension LoadJson {
    
    /// Carrega um arquivo com extensão JSON
    ///
    /// - Parameters:
    ///   - path: caminho do arquivo JSON
    ///   - bundleClass: Bundle de onde está o arquivo
    /// - Returns: Retorna um dicionario com as informações do JSON
    func loadJson(with path: String, bundleClass: AnyClass)-> [String: Any]? {
        
        let bundle = Bundle(for: bundleClass)
        guard let file = bundle.url(forResource: path, withExtension: "json") else { return nil }
        do {
            let data = try Data(contentsOf: file)
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else { return nil }
            return json
        }
        catch {
            print("Erro ao carregar json = \(error.localizedDescription)")
            return nil
        }
    }
    
    
    /// Carrega um arquivo com extensão JSON
    ///
    /// - Parameters:
    ///   - path: Caminho do arquivo JSON
    ///   - bundleClass: Buncle de onde está o arquivo
    /// - Returns: Retorna um array de dicionario com as informações do JSON
    func loadList(with path: String, bundleClass: AnyClass)-> [[String: Any]]? {
        
        let bundle = Bundle(for: bundleClass)
        guard let file = bundle.url(forResource: path, withExtension: "json") else { return nil }
        do {
            let data = try Data(contentsOf: file)
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else { return nil}
            return json
        }
        catch {
            print("Erro ao carregar json = \(error.localizedDescription)")
            return nil
        }
    }
}
