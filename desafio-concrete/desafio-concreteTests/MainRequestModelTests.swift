//
//  MainRequestModelTests.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 30/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import XCTest
@testable import desafio_concrete

class MainRequestModelTests: XCTestCase, LoadJson, Parser {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let json = loadJson(with: "MainRequest", bundleClass: MainRequestModelTests.self)
        
        //Faz o teste com um JSON Normal
        XCTAssertNotNil(json, "JSON INVÁLIDO")
        guard let model = parseMainRequest(response: json) else { return }
        XCTAssertNotNil(model, "PARSE DO MAIN ESTÁ INVALIDO")
        //Caso a propriedade do model retorna TRUE, informa que o JSON está incompleto
        XCTAssertFalse(model.incompleteResults, "JSON ESTA INCOMPLETO")
        
        //Testa um arquivo de JSON quebrado, caso não sejá nil a resposta dos dois Asserts, o model está conseguindo parsear um JSON inválido.
        let jsonBroken = loadJson(with: "MainRequestBroken", bundleClass: MainRequestModelTests.self)
        XCTAssertNil(jsonBroken, "JSON VÁLIDO")
        let modelBroken = parseMainRequest(response: jsonBroken)
        XCTAssertNil(modelBroken, "PARSE DE JSON INVALIDO FEITO")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
