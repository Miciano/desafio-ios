//
//  ServerError.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation

struct ServerError {
    let statusCode: Int
    let description: String
}
