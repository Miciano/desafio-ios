//
//  APIURLs.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation

//Struct criada para conter todas as URLs usadas no projeto
struct APIURLs {
    static let Main: String = "https://api.github.com/search/repositories?q=language:Java&sort=stars&"
}
