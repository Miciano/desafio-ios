//
//  ResponseService.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit

//Tipos de sucesso e erro usadas no request de Repositorio
enum MainResponse {
    case success(model: MainRequestModel?)
    case serverError(description: ServerError)
    case timeOut(description: ServerError)
    case noConnection(description: ServerError)
    case invalidResponse
}

//Tipos de sucesso e erro usadas no request de PullRequests
enum PullResponse {
    case success(model: [PullModel]?)
    case serverError(description: ServerError)
    case timeOut(description: ServerError)
    case noConnection(description: ServerError)
    case invalidResponse
    case empty
}
