//
//  RepositoriesViewController.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress

class RepositoriesViewController: UIViewController, Dismissable {
    
    public enum Action {
        case selectedRepositorie(repositorie: RepositoriesModel)
    }
    
    //PRAGMA MARK: -- VARIAVEIS PUBLICAS --
    
    var dismissAction: (RepositoriesViewController.Action, UIViewController)-> Void = {_ in}
    
    lazy var request = {
        return RequestService()
    }()
    
    //Mascara da View
    var tableView: UITableView {
        return self.view as! UITableView
    }
    
    //MARK: -- VARIAVEIS PRIVADAS --
    fileprivate var page = 0
    fileprivate var mainModel: MainRequestModel?
    fileprivate var dataSource = [RepositoriesModel]()
    
    init() {
        super.init(nibName: "RepositorieView", bundle: Bundle.main)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "GitHub JavaPop"
        
        //Configuro a tableView
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableView.register(UINib(nibName: CellIdentifiers.load.rawValue, bundle: Bundle.main),
                           forCellReuseIdentifier: CellIdentifiers.load.rawValue)
        tableView.register(UINib(nibName: CellIdentifiers.repositorie.rawValue, bundle: Bundle.main),
                           forCellReuseIdentifier: CellIdentifiers.repositorie.rawValue)
        
        tableView.estimatedRowHeight = 150.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        //Mando fazer o load de repositorios
        KVNProgress.show(withStatus: "Carregando...")
        loadRepositories()
    }
    
    //PRAGMA MARK: -- ACTIONS --
    //Função chamada no momento do pull refresh
    func refreshAction() {
        page = 0
        mainModel = nil
        dataSource.removeAll()
        loadRepositories()
    }
    
    //PRAGMA MARK: -- REQUEST LOAD --
    fileprivate func loadRepositories() {
        
        page += 1
        
        request.loadRepositories(page: page) {[weak self] (response) in
            KVNProgress.dismiss()
            switch response {
            case .success(let model):
                self?.mainModel = model
                self?.incrementResults()
            case .serverError(let description):
                UIAlertController.alert(title: "Erro \(description.statusCode)", message: description.description, presenter: self, cancelButton: false, handler: nil)
            case .noConnection(let description):
                if self?.tableView.refreshControl?.isRefreshing == true { self?.tableView.refreshControl?.endRefreshing() }
                UIAlertController.alert(title: "Atenção", message: description.description, presenter: self, cancelButton: false, handler: nil)
            case .timeOut(let description):
                UIAlertController.alert(title: "Atenção", message: description.description, presenter: self, cancelButton: false, handler: { _ in
                    KVNProgress.show(withStatus: "Carregando...")
                    self?.refreshAction()
                })
            case .invalidResponse:
                UIAlertController.alert(title: "Erro", message: "Não prevemos esse caso, irei causar um fatalError", presenter: self, cancelButton: false, handler: {_ in
                    fatalError("-- X response inválido X --")
                })
            }
        }
    }
    
    //Função incrementa o DataSource para uso na tableView
    func incrementResults() {
        //Faço unwrap dos items
        guard let items = mainModel?.items else { return }
        
        if dataSource.count == 0 {
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
        
        //Adiciono os items no dataSource
        for item in items {
            dataSource.append(item)
        }
        
        if self.tableView.refreshControl?.isRefreshing == true { self.tableView.refreshControl?.endRefreshing() }
        //Falo para tabela se recarregar
        self.tableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//PRAGMA MARK: -- TABLE VIEW DATA SOURCE --
extension RepositoriesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row > dataSource.count {
            return tableView.emptyCell()
        }
        
        //Se for a ultima celula retorno a celula de load
        if indexPath.row == dataSource.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.load.rawValue, for: indexPath) as? LoadViewCell else { return tableView.emptyCell()}
            cell.activityView.startAnimating()
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.repositorie.rawValue, for: indexPath) as? RepositoriesViewCell else {
            //Se der erro ao achar a celula de repositorio eu retorno uma celula vazia
            return tableView.emptyCell()
        }
        
        cell.configure(with: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainModel?.total == dataSource.count ? dataSource.count : dataSource.count+1
    }
}

//PRAGMA MARK: -- TABLE VIEW DELEGATE --
extension RepositoriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Se for a ultima celula carrega mais repositorios
        if indexPath.row == dataSource.count {
            loadRepositories()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let repositorie = dataSource[indexPath.row]
        callDismiss(.selectedRepositorie(repositorie: repositorie))
    }
}
