//
//  PullRequestViewController.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress

class PullRequestViewController: UIViewController, Dismissable {
    
    public enum Action {
        case selectedCell(pull: PullModel)
    }
    
    //PRAGMA MARK: -- VARIAVEIS PUBLICAS --
    var dismissAction: (PullRequestViewController.Action, UIViewController)-> Void = {_ in}
    
    //Mascara da view
    var tableView: UITableView {
        return self.view as! UITableView
    }
    
    lazy var request = {
        return RequestService()
    }()
    
    //PRAGMA MARK: -- VARIAVEIS PRIVADAS --
    fileprivate var dataSource = [PullModel]()
    fileprivate var repositorie: RepositoriesModel
    fileprivate var page = 0
    fileprivate var empty = false
    
    init(with repositorie: RepositoriesModel) {
        self.repositorie = repositorie
        super.init(nibName: "PullRequestView", bundle:  Bundle.main)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = repositorie.name
        
        //Configuro a tableView
        tableView.register(UINib(nibName: CellIdentifiers.load.rawValue, bundle: Bundle.main),
                           forCellReuseIdentifier: CellIdentifiers.load.rawValue)
        self.tableView.register(UINib(nibName: CellIdentifiers.pullRequest.rawValue, bundle: Bundle.main),
                                forCellReuseIdentifier: CellIdentifiers.pullRequest.rawValue)
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableView.estimatedRowHeight = 150.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        //Peço para carregar os pullsReqeust
        KVNProgress.show(withStatus: "Carregando...")
        loadPullRequest()
    }
    
    //PRAGMA MARK: -- ACTIONS --
    //Função chamada no pull refresh da tabela
    func refreshAction() {
        dataSource.removeAll()
        empty = false
        page = 0
        loadPullRequest()
    }
    
    //PRAGMA MARK: -- LOAD REQUEST --
    func loadPullRequest() {
        
        page += 1
        
        //Removo o {/number} que vem na url e adiciono ?state=all para fazer o request
        let url = repositorie.pullsURL.characters.split(separator: "{").map(String.init)
        request.loadPullRequest(url: "\(url[0])?state=all&page=\(page)") {[weak self] (response) in
            KVNProgress.dismiss()
            switch response {
            case .success(let model):
                guard let model = model else { return }
                self?.incrementDataSource(model: model)
            case .serverError(let description):
                UIAlertController.alert(title: "Erro \(description.statusCode)", message: description.description, presenter: self, cancelButton: false, handler: nil)
            case .noConnection(let description):
                UIAlertController.alert(title: "Atenção", message: description.description, presenter: self, cancelButton: false, handler: nil)
                if self?.tableView.refreshControl?.isRefreshing == true { self?.tableView.refreshControl?.endRefreshing() }
            case .timeOut(let description):
                UIAlertController.alert(title: "Atenção", message: description.description, presenter: self, cancelButton: false, handler: { _ in
                    KVNProgress.show(withStatus: "Carregando...")
                    self?.refreshAction()
                })
            case .empty:
                self?.empty =  true
                self?.tableView.reloadData()
            case .invalidResponse:
                UIAlertController.alert(title: "Erro", message: "Não prevemos esse caso, irei causar um fatalError", presenter: self, cancelButton: false, handler: {_ in
                    fatalError("-- X response inválido X --")
                })
            }
        }
    }
    
    
    /// Função incrementa o dataSource da tabela a cada pagina carregada
    ///
    /// - Parameter model: ultimo PullModel recebido do request
    fileprivate func incrementDataSource(model: [PullModel]) {
        if tableView.refreshControl?.isRefreshing == true { tableView.refreshControl?.endRefreshing() }
        
        if dataSource.count == 0 {
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
        
        for pull in model { dataSource.append(pull) }
        
        tableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//PRAGMA MARK: -- TABLE VIEW DATA SOURCE --
extension PullRequestViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return empty == true ? dataSource.count : dataSource.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row > dataSource.count { return tableView.emptyCell() }
        
        if indexPath.row == dataSource.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.load.rawValue, for: indexPath) as? LoadViewCell else { return tableView.emptyCell()}
            cell.activityView.startAnimating()
            return cell
        }
        
        let pull = dataSource[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.pullRequest.rawValue, for: indexPath) as? PullViewCell else { return tableView.emptyCell() }
        
        cell.configure(with: pull)
        
        return cell
    }
}

//PRAGMA MARK: -- TABLE VIEW DELEGATE --
extension PullRequestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        callDismiss(.selectedCell(pull: dataSource[indexPath.row]))
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Se for a ultima celula carrega mais pullRequests
        if indexPath.row == dataSource.count {
            loadPullRequest()
        }
    }
}
