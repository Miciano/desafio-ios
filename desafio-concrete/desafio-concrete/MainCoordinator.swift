//
//  MainCoordinator.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit

struct MainCoordinator: CoordinatorProtocol {
    
    var navigationController: UINavigationController
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    //Inicia a tela de listagem de repositorios
    func startRepositories() {
        let controller = RepositoriesViewController()
        controller.dismissAction = { (action, viewController) in
            switch action {
            case .selectedRepositorie(let repositorie):
                self.startPullRequests(repositorie: repositorie)
            }
        }
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    //Inicia a tela de listagem de pullRequests
    func startPullRequests(repositorie: RepositoriesModel) {
        let controller = PullRequestViewController(with: repositorie)
        controller.dismissAction = {(action, viewController) in
            switch action {
            case .selectedCell(let pull):
                self.startWebView(with: pull)
            }
        }
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    //Inicia a tela de webView que abre o pullRequest
    func startWebView(with pull: PullModel) {
        let controller = WebViewController(with: pull)
        self.navigationController.pushViewController(controller, animated: true)
    }
}
