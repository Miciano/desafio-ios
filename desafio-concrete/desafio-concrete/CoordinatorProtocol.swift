//
//  CoordinatorProtocol.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Tikal Tech. All rights reserved.
//

import Foundation
import UIKit

/***
 =========================================================================================
 PROTOCOLO USADO EM TODOS OS COORDINATOS, COM IMPLEMENTACAO DO INIT SENDO OBRIGATORIA
 =========================================================================================
***/

protocol CoordinatorProtocol {
    
    var navigationController: UINavigationController { get }
    
    init(with navigationController: UINavigationController)
}
