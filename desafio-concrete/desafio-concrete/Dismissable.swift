//
//  Dismissable.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//
import Foundation
import UIKit

/*
 ====================================================================
 PROTOCOLO USADO PARA CONTROLE DE ACAO DE FILHOS PARA PAI
 ====================================================================
 */

protocol Dismissable {
    
    associatedtype DismissableAction
    associatedtype DismissableObject
    var dismissAction: ((DismissableAction, DismissableObject)->Void) { get set }
}

extension Dismissable where Self:UIViewController, DismissableObject == UIViewController {
    func callDismiss(_ action: DismissableAction) {
        self.dismissAction(action, self)
    }
}
