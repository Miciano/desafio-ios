//
//  WebViewController.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress

class WebViewController: UIViewController {
    
    //Mascara da view
    var webView: UIWebView {
        return self.view as! UIWebView
    }
    
    fileprivate let pull: PullModel
    
    init(with pull: PullModel) {
        self.pull = pull
        super.init(nibName: "WebView", bundle: Bundle.main)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Pull #\(pull.id)"
        
        guard let urlPull = pull.urlPull,
            let url = URL(string: urlPull) else { return }
        let urlRequest = URLRequest(url: url)
        KVNProgress.show(withStatus: "Carregando...")
        webView.delegate = self
        //Carrego a url
        webView.loadRequest(urlRequest)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//PRAGMA MARK: -- WEBVIEW DELEGATE --
extension WebViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        KVNProgress.dismiss()
    }
}
