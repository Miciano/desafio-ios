//
//  RepositoriesViewCell.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit

class RepositoriesViewCell: UITableViewCell {
    
    @IBOutlet weak var repositorieLb: UILabel!
    @IBOutlet weak var descriptionLb: UILabel!
    @IBOutlet weak var forkLb: UILabel!
    @IBOutlet weak var startLb: UILabel!
    @IBOutlet weak var userNameLb: UILabel!
    @IBOutlet weak var imageProfile: ImageProfile!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageProfile.clean()
    }
    
    /// Função de configuração da celula de repositorio
    ///
    /// - Parameter repositorie: Modelo que contem todas as informações do repositorio
    func configure(with repositorie: RepositoriesModel) {
        
        repositorieLb.text = repositorie.name
        descriptionLb.text = repositorie.description
        forkLb.text = "\(repositorie.forks)"
        startLb.text = "\(repositorie.stargazersCount)"
        userNameLb.text = repositorie.owner?.login
        imageProfile.createPhoto(with: repositorie.owner?.avatarUrl, handlerError: nil)
    }
}
