//
//  ImageProfile.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 30/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import SnapKit

class ImageProfile: UIView {
    
    let imageView = UIImageView()
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        createActivityView()
        createCircleImage()
    }
    
    //Função faz o request para carregar a imagem
    func createPhoto(with url: String?, handlerError:((_ description: ServerError)->Void)?) {
        guard let url = url,
            let urlRequest = URL(string: url) else { return }
        //Request de download da imagem
        imageView.af_setImage(withURL: urlRequest, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true) {[weak self] (response) in
            switch response.result
            {
            case .success( _):
                self?.insertPhoto()
            case .failure(let error):
                switch error._code {
                case -999, 0:
                    self?.createPhoto(with: url, handlerError: nil)
                default:
                    print("-- X Nao foi possivel fazer download da imagem \(url) X --")
                }
            }
        }
    }
    
    //Função remove todos os filhos da View
    func clean() {
        imageView.image = nil
        activity.isHidden = false
        self.backgroundColor = .lightGray
    }
    
    //PRAGMA MARK: -- FUNÇÕES PRIVADAS --
    //Função cria um activityView para mostrar para usuário que a imagem está em processo de load
    fileprivate func createActivityView() {
        //Mudo a cor do background
        self.backgroundColor = UIColor.lightGray
        //Adiciono o activity na view
        self.addSubview(activity)
        activity.startAnimating()
        //Adiciono constraints nele para que ele fique centralizado na celula
        activity.snp.makeConstraints { (make) in
            make.centerX.equalTo(0)
            make.centerY.equalTo(0)
        }
    }
    
    //Função faz com que a View fique circular
    fileprivate func createCircleImage() {
        //Verifica se tem uma constraint de largura
        let constraintWidth = self.constraints.filter { (constraint) -> Bool in
            return constraint.firstAttribute == .width ? true : false
        }
        
        //Se tiver constraint usa ela para o calculo de arredondamento, se nao usa o tamanho do frame
        self.layer.cornerRadius =  constraintWidth.count > 0 ? constraintWidth[0].constant * 0.5  : self.frame.size.width * 0.5
        self.clipsToBounds = true
    }
    
    //Função insere a foto na View
    fileprivate func insertPhoto() {
        //Removo o ActivityView
        self.activity.isHidden = true
        self.backgroundColor = .clear
        //Insiro a imagem como primeiro filho da view
        self.insertSubview(imageView, at: 0)
        //Adiciono constraints pra que a imagem fique do mesmo tamanho da view
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.leading.equalTo(0)
            make.trailing.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
}
