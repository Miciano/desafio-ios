//
//  TableViewExtension.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 29/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    
    /// Função cria uma tableViewCell vazia
    ///
    /// - Returns: celula vazia
    func emptyCell()-> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "empty")
        return cell
    }
}
