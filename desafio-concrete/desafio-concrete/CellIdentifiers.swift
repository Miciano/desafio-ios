//
//  CellIdentifiers.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 30/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation

//Enum usado para encapsular todas as Strings de Cell Idenfiers usada no código
enum CellIdentifiers: String {
    case repositorie = "RepositorieViewCell"
    case pullRequest = "PullViewCell"
    case load = "LoadViewCell"
}
