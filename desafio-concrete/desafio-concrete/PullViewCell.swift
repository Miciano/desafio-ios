//
//  PullViewCell.swift
//  desafio-concrete
//
//  Created by Fabio Miciano on 30/07/17.
//  Copyright © 2017 Fabio Miciano. All rights reserved.
//

import Foundation
import UIKit

class PullViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var descriptionLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var statusLb: UILabel!
    @IBOutlet weak var imageProfile: ImageProfile!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageProfile.clean()
    }
    
    /// Função de configuração da Celula de PullRequest
    ///
    /// - Parameter pull: Modelo que contem todas as informações do PullRequest
    func configure(with pull: PullModel) {
        titleLb.text = pull.title
        descriptionLb.text = pull.body
        nameLb.text = pull.user?.login
        statusLb.text = "Status: \(pull.status)"
        guard let url = pull.user?.avatarUrl else { return }
        imageProfile.createPhoto(with: url, handlerError: nil)
    }
}
